package beryl

import (
	"log"
	"reflect"
	"testing"
)

func TestLoadFromFilenameDoesntCrash(t *testing.T) {
	if _, err := LoadFromFilename("testData/test1.md"); err != nil {
		log.Fatal(err)
	}
}

func TestParseCompleted(t *testing.T) {
	tasks, err := LoadFromFilename("testData/test1.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Completed != false {
		log.Fatal("bad1")
	}
}

func TestParseCompleted2(t *testing.T) {
	tasks, err := LoadFromFilename("testData/test2.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Completed != true {
		log.Fatal("bad2")
	}
}

func TestParseTitle(t *testing.T) {
	tasks, err := LoadFromFilename("testData/test1.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Title != "task title" {
		log.Fatal("bad3")
	}
}

func TestParseTwoTasks(t *testing.T) {
	tasks, err := LoadFromFilename("testData/test3.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Title != "task title" {
		log.Println(tasks[0].Title)
		log.Fatal("bad second task title")
	}
	if tasks[1].Title != "task title2" {
		log.Fatal("bad5")
	}
}

func TestParseTags(t *testing.T) {
	tasks, err := LoadFromFilename("testData/test1.md")
	if err != nil {
		log.Fatal(err)
	}

	testTags := map[string]string{"completed": "date", "context": "contextText", "created": "date", "due": "date", "p": "1"}

	if !reflect.DeepEqual(testTags, tasks[0].Tags) {
		log.Fatal("bad tags")
	}
}

func TestParseComment(t *testing.T) {
	tasks, err := LoadFromFilename("testData/comment.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Comment != "this is a test comment" {
		log.Printf(tasks[0].Comment)
		log.Fatal("bad comment parsing")
	}
}

func TestParseMultilineComment(t *testing.T) {
	tasks, err := LoadFromFilename("testData/comment2.md")
	if err != nil {
		log.Fatal(err)
	}

	multiLine := "this is a test comment\nwith two lines"

	parsedComment := tasks[0].Comment

	if parsedComment != multiLine {

		log.Fatal("bad multiline parsing")
	}
}

func TestParseMultiTaskComment(t *testing.T) {
	tasks, err := LoadFromFilename("testData/comment3.md")
	if err != nil {
		log.Fatal(err)
	}

	multiLine := "this is a test comment\nwith two lines"

	parsedComment := tasks[0].Comment

	if parsedComment != multiLine {

		log.Fatal("bad multiline parsing")
	}

	parsedComment = tasks[1].Comment

	if parsedComment != multiLine {

		log.Fatal("bad multiline parsing")
	}
}

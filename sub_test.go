package beryl

import (
	"log"
	"testing"
)

func TestCountTabsWorks(t *testing.T) {
	res := CountTabs("\t\t")
	if res != 2 {
		log.Fatal("bad tabs")
	}

	res = CountTabs(`	- [x] task title2`)
	if res != 1 {
		log.Fatal("bad tabs")
	}
}

func TestParseSingleSubtask(t *testing.T) {
	// one task with one subtask
	tasks, err := LoadFromFilename("testData/sub.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].Completed != false {
		log.Fatal("bad1")
	}

	if tasks[0].SubTasks[0].Completed != true {
		log.Fatal("bad subtask")
	}

}

func TestParseMultipleSubtask(t *testing.T) {
	// one task with two subtask
	tasks, err := LoadFromFilename("testData/sub2.md")
	if err != nil {
		log.Fatal(err)
	}

	if tasks[0].SubTasks[0].Title != "task title2" {
		log.Println(tasks[0].SubTasks[0].Title)
		log.Fatal("first subtask fail")
	}

	if tasks[0].SubTasks[1].Title != "task title3" {
		log.Println(tasks[0].SubTasks[1].Title)
		log.Fatal("first subtask fail")
	}

}

func TestParseMultipleSuperTasks(t *testing.T) {
	// two task with two subtask
	tasks, err := LoadFromFilename("testData/sub3.md")
	if err != nil {
		log.Fatal(err)
	}

	// log.Println(tasks)

	if tasks[0].SubTasks[0].Title != "task title2" {
		log.Println(tasks[0].SubTasks[0].Title)
		log.Fatal("first subtask fail")
	}

	if tasks[0].SubTasks[1].Title != "task title3" {
		log.Println(tasks[0].SubTasks[1].Title)
		log.Fatal("first subtask fail")
	}

	if tasks[1].SubTasks[0].Title != "task title5" {
		// log.Println(tasks[0].SubTasks[0].Title)
		log.Fatal("first subtask fail")
	}

	if tasks[1].SubTasks[1].Title != "task title6" {
		// log.Println(tasks[0].SubTasks[1].Title)
		log.Fatal("first subtask fail")
	}

}

func TestStress(t *testing.T) {
	tasks, err := LoadFromFilename("testData/stress.md")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(tasks)
	if tasks[1].Title != "task title4" {
		log.Println("bad task 4")
		log.Fatal(err)
	}

	log.Println(tasks)
}

func TestStress2(t *testing.T) {
	tasks, err := LoadFromFilename("testData/stress2.md")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(tasks)
	if tasks[1].Title != "task title4" {
		log.Println("bad task 4")
		log.Fatal(err)
	}

	log.Println(tasks)
}

func TestWriteStress(t *testing.T) {
	tasks, err := LoadFromFilename("testData/stress.md")
	if err != nil {
		log.Fatal(err)
	}

	WriteToFilename(&tasks, "testData/out.md")

}
